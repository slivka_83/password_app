#import libraries
import numpy as np
import pandas as pd
from flask import Flask, render_template, request, url_for
import joblib
import matplotlib.pyplot as plt
import shap
import lightgbm as lgbm
from model.functions import preproc, log_unproc

def shap_plot(sv):
    p = shap.plots.waterfall(sv, max_display=10, show=False)
    plt.savefig('static/explain.png', bbox_inches='tight')
    plt.close()
    return p

def model_predict(pw):
    df = pd.DataFrame([{'Password':pw}])
    df = preproc(df)
    df_feat = df.iloc[:,1:]
    pred = log_unproc(model.predict(df_feat))[0]

    shap_values = explainer(df_feat)
    shap_plot(shap_values[0])

    return round(pred,2)


model = joblib.load('model/model.joblib')
explainer = joblib.load(filename='model/explainer.joblib')

# Flask instance
app = Flask(__name__)


@app.route("/", methods=["POST", "GET"])
def index():
    """Main form rendering"""
    if request.method == 'POST':
        pw = request.form["password"]
        pass_freq = model_predict(pw)
        return render_template("index.html", password=pw, prediction=pass_freq)
    else:
        return render_template("index.html")


if __name__ == "__main__":
    # for development set "debug=True"in app.run
    app.run(host="0.0.0.0", threaded=False, debug=True)