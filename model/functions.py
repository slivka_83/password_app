import pandas as pd
import numpy as np

def preproc(df):
    df['len'] = df['Password'].str.len().fillna(0).astype(int)
    df['have_num'] = df['Password'].str.contains('\d').fillna(0).astype(int)
    df['cnt_num'] = df['Password'].str.count('\d').fillna(0).astype(int)
    df['have_up_case'] = df['Password'].str.contains('[A-Z]').fillna(0).astype(int)
    df['cnt_up_case'] = df['Password'].str.count('[A-Z]').fillna(0).astype(int)
    df['have_low_case'] = df['Password'].str.contains('[a-z]').fillna(0).astype(int)
    df['cnt_low_case'] = df['Password'].str.count('[a-z]').fillna(0).astype(int)
    df['have_spec'] = (df['Password'].str.contains('^\w+$').fillna(0) == False).astype(int)
    df['cnt_spec'] = df['len'] - df['cnt_num'] - df['cnt_up_case'] - df['cnt_low_case']
    df['len_eq_num'] = (df['len'] == df['cnt_num']).astype(int)
    df['len_eq_up_case'] = (df['len'] == df['cnt_up_case']).astype(int)
    df['len_eq_low_case'] = (df['len'] == df['cnt_low_case']).astype(int)
    
    df['len_8'] = 0
    df.loc[df['len']==8,'len_8'] = 1
    df['isdigit']=df['Password'].fillna(0).str.isdigit().fillna(0).astype(int)
    df['d_b'] = (df['isdigit']==1)&(df['len_8']==1).astype(int)
    df['d_b_y'] = df[df['d_b']==1]['Password'].str[4:].astype(int)
    df['d_b_y'].fillna(0,inplace=True)
    df['d_b_y'] =df['d_b_y'].astype(int)
    df.loc[(df['d_b_y']<1960)|(df['d_b_y']>2010),'d_b_y'] = 0
    df.drop('d_b',axis=1,inplace=True)

    return df

def log_proc(s):
    return np.log(s)

def log_unproc(s):
    return np.exp(s)
