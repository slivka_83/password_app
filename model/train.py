import pandas as pd
import lightgbm as lgbm
import joblib
from sklearn.model_selection import train_test_split, ParameterSampler, ParameterGrid
import numpy as np

def preproc(df):
    df['len'] = df['Password'].str.len().fillna(0).astype(int)
    df['have_num'] = df['Password'].str.contains('\d').fillna(0).astype(int)
    df['cnt_num'] = df['Password'].str.count('\d').fillna(0).astype(int)
    df['have_up_case'] = df['Password'].str.contains('[A-Z]').fillna(0).astype(int)
    df['cnt_up_case'] = df['Password'].str.count('[A-Z]').fillna(0).astype(int)
    df['have_low_case'] = df['Password'].str.contains('[a-z]').fillna(0).astype(int)
    df['cnt_low_case'] = df['Password'].str.count('[a-z]').fillna(0).astype(int)
    df['have_spec'] = (df['Password'].str.contains('^\w+$').fillna(0) == False).astype(int)
    df['cnt_spec'] = df['len'] - df['cnt_num'] - df['cnt_up_case'] - df['cnt_low_case']
    df['len_eq_num'] = (df['len'] == df['cnt_num']).astype(int)
    df['len_eq_up_case'] = (df['len'] == df['cnt_up_case']).astype(int)
    df['len_eq_low_case'] = (df['len'] == df['cnt_low_case']).astype(int)
    
    df['len_8'] = 0
    df.loc[df['len']==8,'len_8'] = 1
    df['isdigit']=df['Password'].fillna(0).str.isdigit().fillna(0).astype(int)
    df['d_b'] = (df['isdigit']==1)&(df['len_8']==1).astype(int)
    df['d_b_y'] = df[df['d_b']==1]['Password'].str[4:].astype(int)
    df['d_b_y'].fillna(0,inplace=True)
    df['d_b_y'] =df['d_b_y'].astype(int)
    df.loc[(df['d_b_y']<1960)|(df['d_b_y']>2010),'d_b_y'] = 0
    df.drop('d_b',axis=1,inplace=True)

    return df

def log_proc(s):
    return np.log(s)

def log_unproc(s):
    return np.exp(s)

def rmsle_lgbm(y_pred, data):
    y_true = log_unproc(np.array(data.get_label()))
    y_pred = log_unproc(y_pred)
    score = np.sqrt(np.mean(np.power(np.log1p(y_true) - np.log1p(y_pred), 2)))
    return 'rmsle', score, False

print('- Загрузка данных')
train = pd.read_csv('train.csv')

print('- Препроцессинг')
train['Times'] = log_proc(train['Times'])
train = preproc(train)

print('- Разбиение данных')
feat = list(train.columns[2:])

X_train, X_test, y_train, y_test = train_test_split(train[feat], train['Times'], test_size = 0.2)
train_set = lgbm.Dataset(X_train, y_train)
valid_set = lgbm.Dataset(X_test, y_test)

print('- Обучение')
params = {
    'max_depth': range(4,11+1),
    'learning_rate': np.logspace(-2,-1,10)}
ps = list(ParameterSampler(params, n_iter=10, random_state=42))
ps.sort(key=lambda row: (row['max_depth'],row['learning_rate']), reverse=False) 

best_score = 100
best_model = None

for p in ps:
    md = p['max_depth']
    lr = p['learning_rate']
    
    param = {
        'objective': 'regression',
        'n_jobs': 5,
        'verbose': -1,
        'max_depth': md,
        'learning_rate': lr}

    bst = lgbm.train(
        param, 
        train_set,
        valid_sets = valid_set,
        early_stopping_rounds = 30,
        num_boost_round = 2000,
        feval = rmsle_lgbm,
        verbose_eval = None
    )
    
    best_iter = bst.best_iteration
    score = bst.best_score['valid_0']['rmsle']
    
    if score < best_score:
        best_score = score
        best_score_mark = '<--'
        best_model = bst
    else:
        best_score_mark = ''
    
    print(f'md:', md, '| lr:', round(lr,4), '| iter:', best_iter, '| score:', round(score,5), best_score_mark)


joblib.dump(best_model, "model.joblib")